import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.util.Scanner;
public class LoginPanel extends JPanel 
{
   private JPanel logPanel;
   private JLabel labelUser;
   private JLabel labelPass;
   private JButton confirm;
   private JTextField username;
   private JTextField password;
   private PlayerList publicList;
   private LoginInterface Interface;
   
   public LoginPanel(PlayerList list, LoginInterface L)
   {
      Interface = L;
      publicList = list;
      logPanel = new JPanel();
      logPanel.setLayout(new GridLayout(3,2));
      
      labelUser = new JLabel("Username");
      labelUser.setFont(new Font("Serif", Font.ITALIC + Font.BOLD, 20));
      
      labelPass = new JLabel("Password");
      labelPass.setFont(new Font("Serif", Font.ITALIC + Font.BOLD, 20));
      
      confirm = new JButton("Confirm");
      confirm.addActionListener(new Listener());
      
      username = new JTextField(20);
      password = new JTextField(20);
      
      logPanel.add(labelUser);
      logPanel.add(username);
      logPanel.add(labelPass);
      logPanel.add(password);
      logPanel.add(confirm);
      
      add(logPanel);
      setVisible(true);
   }
   public void True(Player play)
   {
   } 
   public void finish(Player player)
   {
      Interface.setPlayer(player);
   }
   private class Listener implements ActionListener
   { 
      public void actionPerformed(ActionEvent e)
      {
         if (e.getSource() == confirm)
         { 
            String P = username.getText();
            boolean id = publicList.containsId(P);
            
            if (id)
            {
               JOptionPane.showMessageDialog(logPanel, "Id has been found" );
            }
            else
            {
               JOptionPane.showMessageDialog(logPanel, "Id has not been found");
            }
            String pass = password.getText();
            Player player = new Player(P);
            player.setPass(pass);
            Player temp = new Player(P);
            temp.setPass(pass);
            Player play = publicList.authenticate(temp);
            
            if (play != null)
            {
               finish(play);
            }
            else
            {
               JOptionPane.showMessageDialog(logPanel, "Invalid password");
            }
         } 
      }
   }
}