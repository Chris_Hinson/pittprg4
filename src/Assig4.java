// CS 0401 2018
// 
// This program tests all 3 of the Assignment 4 required classes in a simple
// graphical implementation of the Word Finder Game.  If your WordPanel,
// LoginPanel and GamePanel (as well as Player and PlayerList from
// Assignment 3) classes are all written correctly then this program
// should also run correctly.

// See more comments below.

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Assig4 implements GameInterface, LoginInterface
{
	// Note the variables -- we have variables for the window and all of
	// the various panels.  Not all of these will be visible at the same
	// time however.  We also have 3 buttons which will initially be shown
	// to the user.
	private JFrame theWindow;
	private WordPanel leftPanel, rightPanel;
	private LoginPanel theLogin;
	private GamePanel theGame;
	private JPanel buttonPanel;
	private JButton quitButton, loginButton, playButton;
	private Player P;
	private PlayerList PL;
	
	public Assig4()
	{
		theWindow = new JFrame("Java Word Finder Program");
      try
      {
		   PL = new PlayerList("C:\\Users\\perso\\OneDrive\\Desktop\\MyPittPrg4\\src\\players.txt");
      }
      catch(IOException error)
      {
         error.printStackTrace();
      }

		loginButton = new JButton("Player Login");
		loginButton.setFont(new Font("Serif", Font.BOLD, 25));
		playButton = new JButton("Start Game");
		playButton.setFont(new Font("Serif", Font.BOLD, 25));
		playButton.setEnabled(false); // cannot play until we have a Player
		quitButton = new JButton("Quit");
		quitButton.setFont(new Font("Serif", Font.BOLD, 25));
		
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(3,1));
		buttonPanel.add(quitButton);
		buttonPanel.add(loginButton);
		buttonPanel.add(playButton);
		
		ActionListener theListener = new GameListener();
		quitButton.addActionListener(theListener);
		loginButton.addActionListener(theListener);
		playButton.addActionListener(theListener);
		
		theWindow.setLayout(new GridLayout(1,3));
		theWindow.add(buttonPanel);

		theWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		theWindow.setSize(300, 400);
		theWindow.setVisible(true);
		
	}
	
	// This single listener will handle the 3 buttons in the buttonPanel.
	// Note the logic.  Initially, playButton is not enabled because there
	// is no Player.  loginButton creates a LoginPanel to allow a new Player
	// to login.  That panel will call setPlayer() when it finishes, which
	// will enable the playButton for that player.   playButton will create
	// the two WordPanel objects and the GamePanel so that the current Player
	// can play the game.  When the Player finishes, the GamePanel will call
	// the gameOver() method.  This will reset the buttons so that a new
	// Player will have to login to play.  It will also set the Player to null
	// (so even if the same Player wants to play twice in a row, he / she
	// would have to login again to do it).
	private class GameListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() == loginButton)
			{
				if (P != null)
				{
					JOptionPane.showMessageDialog(theWindow, "Player " + P.getUniqueID() +
						" quitting without playing ");
				}
				theLogin = new LoginPanel(PL, Assig4.this);
				theWindow.remove(buttonPanel);
				theWindow.add(theLogin);
				theWindow.pack();
			}
			else if (e.getSource() == playButton)
			{
				leftPanel = new WordPanel(10, 18, "Found Words");
				rightPanel = new WordPanel(10, 18, "Guessed Words");
				leftPanel.setFontSize(25);
				rightPanel.setFontSize(25);
				theGame = new GamePanel(P, leftPanel, rightPanel, Assig4.this);
				theWindow.remove(buttonPanel);
				theWindow.add(leftPanel);
				theWindow.add(rightPanel);
				theWindow.add(theGame);
				theWindow.pack();
			}
			else if (e.getSource() == quitButton)
			{
				if (P != null)
				{
					JOptionPane.showMessageDialog(theWindow, "Player " + P.getUniqueID() +
						" quitting without playing ");
				}
            try
            {
				   PL.saveList("C:\\Users\\perso\\OneDrive\\Desktop\\MyPittPrg4\\src\\players.txt");
            }
            catch(IOException error)
            {
               error.printStackTrace();
            }
            JOptionPane.showMessageDialog(theWindow, "Overall results:\n" + PL.toString());
				System.exit(0);
			}		
		}
	}
	
	public void setPlayer(Player pl)
	{
		P = pl;
		playButton.setEnabled(true);
		playButton.setText(P.getUniqueID() + " Start Game ");
		theWindow.remove(theLogin);
		theWindow.add(buttonPanel);
		JOptionPane.showMessageDialog(theWindow, "Ready to play with \n" +
			P.toString());
		theWindow.setSize(300,400);
	}
	
	// This method will be called from the GamePanel when it is finished with the
	// game.  It removes the GamePanel, adds back the buttonPanel and sets the
	// Player P back to null.
	public void gameOver()
	{
		theWindow.remove(theGame);
		theWindow.remove(rightPanel);
		theWindow.remove(leftPanel);
		theWindow.add(buttonPanel);
      PL.addPlayer(P);
		P = null;
		playButton.setText("Start Game");
		playButton.setEnabled(false);
		theWindow.setSize(300,400);
		theWindow.repaint();
      try
      {
         PL.saveList("C:\\Users\\perso\\OneDrive\\Desktop\\MyPittPrg4\\src\\players.txt");
      }
      catch(IOException error)
      {
         error.printStackTrace();
      }
	}
	public static void main(String [] args)
	{
		new Assig4();
	}
}
