import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.util.Scanner;
public class GamePanel extends JPanel
{//variables
   private JLabel statusMessage;
   private JPanel gamePan;
   private JButton exit;
   private JButton nextWord;
   private JLabel wordLabel;
   private JLabel guessWord;
   private JLabel guess;
   private JTextField area;
   private JLabel status;
   private WordFinder wordfinder = new WordFinder("C:\\Users\\perso\\OneDrive\\Desktop\\MyPittPrg4\\src\\dictionary.txt");
   public LinkedList<String> correctWord = new LinkedList<String>();
   PlayerList list;
   private int roundPoints = 0;
   private int roundCounter = 0;
   private int totalPoints = 0;
   private int total = 0;
   private double averagePoints = 0;
   private int rounds = 0;
   private double averageWords = 0;
   private int totalWords = 0;
   private Player P;
   private WordPanel leftPanel;
   private WordPanel rightPanel;
   private GameInterface Interface;
   
   public GamePanel(Player player, WordPanel left, WordPanel right, GameInterface Interface) //gamepanel constructor
   {
      P = player;
      leftPanel = left;
      rightPanel = right;
      this.Interface = Interface;
      
      gamePan = new JPanel();
      gamePan.setLayout(new GridLayout(4,2));
      
      exit = new JButton("Quit");
      exit.addActionListener(new QuitListener());
      
      nextWord = new JButton("Next Word");
      nextWord.addActionListener(new NextWordListener());
      
      wordLabel = new JLabel("Word: ");
      guessWord = new JLabel();
      
      guess = new JLabel("Guess: ");
      area = new JTextField(18);
      area.addActionListener(new SubmitListener());
      
      status = new JLabel("Status");
      statusMessage = new JLabel();
      //adding components to panel
      gamePan.add(nextWord);
      gamePan.add(exit);
      gamePan.add(guess);
      gamePan.add(area);
      gamePan.add(status);
      gamePan.add(statusMessage);
      gamePan.add(wordLabel);
      gamePan.add(guessWord);
      
      add(gamePan);
      
      setVisible(true);
      
      Random rand = new Random();
      int result = rand.nextInt(18);
      wordfinder.nextWord(result);
      
      JOptionPane.showMessageDialog(gamePan, P.getUniqueID() + " Let's begin. Your word is " + wordfinder.showWord());
      guessWord.setText(wordfinder.showWord());
      rounds = rounds + 1;
      roundCounter++;
   }
   public void endRun() //end of game method
   {
      JOptionPane.showMessageDialog(gamePan, P.getUniqueID() + " you have found " + roundCounter + " words and scored " + roundPoints + " points");
      
      P.addRounds(rounds);
      P.addWords(roundCounter);
      P.addPoints(roundPoints);
      
      averageWords = P.getWordsFound() / P.getRounds();
      //P.addAverageWords(averageWords);
      
      averagePoints = P.getPoints() / P.getRounds();
      //P.addAveragePoints(averagePoints);
      
      JOptionPane.showMessageDialog(gamePan, "Overall Score: " + "\n" + P.toString());
      
      rounds = 0;
      roundCounter = 0;
      roundPoints = 0;
      
      leftPanel.clear();
      rightPanel.clear();
      Interface.gameOver();
   }
   public void nxtRound() //method for post game/next round
   {
      JOptionPane.showMessageDialog(gamePan, P.getUniqueID() + " you have found " + roundCounter + " words and scored " + roundPoints + " points");
      
      P.addRounds(rounds);
      P.addWords(roundCounter);
      P.addPoints(roundPoints);
      
      rounds = 0;
      roundCounter = 0;
      roundPoints = 0;
      
      Random rand = new Random();
      int result = rand.nextInt(18);
      
      wordfinder.nextWord(result);
      guessWord.setText(wordfinder.showWord());
      leftPanel.clear();
      rightPanel.clear();
      
      rounds = rounds + 1;
   } //ACTION LISTENERS
   private class QuitListener implements ActionListener
   {
      public void actionPerformed(ActionEvent e)
      {
         if (e.getSource() == exit)
         {
            endRun();
         }
      }
   }
   private class NextWordListener implements ActionListener
   {
      public void actionPerformed(ActionEvent e)
      {
         if (e.getSource() == nextWord)
         {
            nxtRound();
         }
      }
   }
   private class SubmitListener implements ActionListener
   {
      public void actionPerformed(ActionEvent e)
      {
         String word = area.getText();
         if (wordfinder.goodWord(word)&&!leftPanel.containsWord(word))
         {
            statusMessage.setText("\t"+ word + " word is valid");
            roundPoints = roundPoints + word.length();
            roundCounter = roundCounter + 1;
            
            leftPanel.addWord(word);
            rightPanel.addWord(word);
            area.setText("");
         }
         else
         {
            statusMessage.setText("\t" + word + " word is invalid");
            rightPanel.addWord(word);
            area.setText("");
         }
      }
   }
}     
      
   