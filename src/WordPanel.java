import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.util.Scanner;
public class WordPanel extends JPanel
{
   private JPanel wordPanel;
   private JLabel label;
   private JButton sorterButton;
   private JTextArea text;
   public Font font;
   boolean sorted = false;
   ArrayList<String> inputWords = new ArrayList<String>();
   ArrayList<String> sortedWords = new ArrayList<String>();
   
   public WordPanel(int row, int column, String title)
   {
      label = new JLabel(title);
      text = new JTextArea();
      wordPanel = new JPanel();
      sorterButton = new JButton("Sort");
      sorterButton.addActionListener(new Listener());
      text.setColumns(column);
      text.setRows(row);
      text.setEditable(false);
      
      wordPanel.add(label);
      wordPanel.add(text);
      wordPanel.add(sorterButton);
      
      add(wordPanel);
      setVisible(true);
   }
   public void setFontSize(int size)
   {
      sorterButton.setFont(new Font("Seri", Font.PLAIN, size));
   }
   public void sort()
   {
      Collections.sort(sortedWords);
      text.setText(toString(sortedWords));
   }
   public String toString(ArrayList<String> str)
   {
      StringBuilder string = new StringBuilder();
      for (int i = 0; i < str.size(); i++)
      {
         string.append("\n" + str.get(i));
      }
      String finalString = string.toString();
      return finalString;
   }
   public void clear()
   {
      inputWords.clear();
      sortedWords.clear();
      text.setText("");
   }
   public void input()
   {
      text.setText(toString(inputWords));
   }
   public void addWord(String word)
   {
      inputWords.add(word);
      sortedWords.add(word);
      if (sorted = true)
      {
         sort();
      }
      else
      {
         input();
      }
   }
   private class Listener implements ActionListener
   {
      public void actionPerformed(ActionEvent e)
      {
         if (e.getSource() == sorterButton && sorted == true)
         {
            input();
            sorted = false;
            sorterButton.setText("Input");
         }
         else if (e.getSource() == sorterButton && sorted == false)
         {
            sort();
            sorted = true;
            sorterButton.setText("Sort");
         }
      }
   }

   public Boolean containsWord(String word)
   {
      for(int i =0;i<inputWords.size();i++)
      {
         if(word.equals(inputWords.get(i)))
         {
            return Boolean.TRUE;
         }
      }

      return Boolean.FALSE;

   }
}
      
      